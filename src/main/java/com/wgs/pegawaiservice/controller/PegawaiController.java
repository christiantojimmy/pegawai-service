package com.wgs.pegawaiservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.response.ResponseCodeEnum;
import com.wgs.pegawaiservice.dto.request.PegawaiRequestDto;
import com.wgs.pegawaiservice.dto.response.PegawaiResponseDto;
import com.wgs.pegawaiservice.service.PegawaiService;

@RestController
@RequestMapping("/pegawai")
public class PegawaiController {

	@Autowired
	private PegawaiService service;
	
	@GetMapping
	public ResponseEntity<?> getAll() {
		BaseResponseMessage<List<PegawaiResponseDto>> baseResponseMessage = new BaseResponseMessage<>();
		baseResponseMessage.setStatusCode(ResponseCodeEnum.SUCCESS);
		baseResponseMessage.setErrorMessages(null);
		baseResponseMessage.setData(service.viewAll());

		return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable("id") Long id) {
		BaseResponseMessage<PegawaiResponseDto> baseResponseMessage = new BaseResponseMessage<>();
		PegawaiRequestDto request = new PegawaiRequestDto();
		request.setId(id);
		
		if (!service.doView(baseResponseMessage, request)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(baseResponseMessage);
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.VIEW_SUCCESS);
			return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
		}
	}
	
	@PostMapping
	public ResponseEntity<?> create(@Valid @RequestBody PegawaiRequestDto request) {
		BaseResponseMessage<PegawaiRequestDto> baseResponseMessage = new BaseResponseMessage<>();
		
		if(!service.doSave(baseResponseMessage, request)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.SAVE_SUCCESS.getCode());
			baseResponseMessage.setMessage("Data pegawai berhasil ditambahkan");
			baseResponseMessage.setData(request);
			return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable("id") Long id, @Valid @RequestBody PegawaiRequestDto requestDto) {
		BaseResponseMessage<PegawaiRequestDto> baseResponseMessage = new BaseResponseMessage<>();
		requestDto.setId(id);

		if (!service.doUpdate(baseResponseMessage, requestDto)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.UPDATE_SUCCESS);
			baseResponseMessage.setMessage("Data pegawai berhasil diperbaharui");
			baseResponseMessage.setData(requestDto);
			return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") Long id) {
		BaseResponseMessage baseResponseMessage = new BaseResponseMessage();
		
		PegawaiRequestDto request = new PegawaiRequestDto();
		request.setId(id);
		
		if(!service.doDelete(baseResponseMessage, request)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.DELETE_SUCCESS);
			baseResponseMessage.setMessage("Data pegawai berhasil dihapus");
			return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
		}
	}
	
}
