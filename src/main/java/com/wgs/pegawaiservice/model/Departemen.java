package com.wgs.pegawaiservice.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.wgs.base.model.BaseModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table
@Getter 
@Setter 
@ToString
public class Departemen extends BaseModel {

	private String namaDepartemen;
	
	public void setForUpdate(Departemen departemen) {
		this.namaDepartemen = departemen.getNamaDepartemen();
	}
	
}
