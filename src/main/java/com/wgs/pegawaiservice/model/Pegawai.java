package com.wgs.pegawaiservice.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wgs.base.model.BaseModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table
@Getter 
@Setter 
@ToString
public class Pegawai extends BaseModel {

	private String nip;
	private String nama;
	@ManyToOne
	private Jabatan jabatan;
	@ManyToOne
	private Departemen departemen;
	
	public void setForUpdate(Pegawai pegawai) {
		this.nip = pegawai.getNip();
		this.nama = pegawai.getNama();
		this.jabatan = pegawai.getJabatan();
		this.departemen = pegawai.getDepartemen();
	}
	
}
