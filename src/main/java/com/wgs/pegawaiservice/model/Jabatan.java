package com.wgs.pegawaiservice.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.wgs.base.model.BaseModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table
@Getter 
@Setter 
@ToString
public class Jabatan extends BaseModel {

	private String namaJabatan;
	private BigDecimal gajiPokok;
	
	public void setForUpdate(Jabatan jabatan) {
		this.namaJabatan = jabatan.getNamaJabatan();
		this.gajiPokok = jabatan.getGajiPokok();
	}
	
}
