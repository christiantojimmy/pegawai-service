package com.wgs.pegawaiservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class PegawaiResponseDto {

	private Long id;
	private String nip;
	private String nama;
	private JabatanResponseDto jabatan;
	private DepartemenResponseDto departemen;
	
}
