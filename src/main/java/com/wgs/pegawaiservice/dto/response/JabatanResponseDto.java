package com.wgs.pegawaiservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class JabatanResponseDto {

	private Long id;
	private String namaJabatan;
	private String gajiPokok;
	
}
