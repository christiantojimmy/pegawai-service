package com.wgs.pegawaiservice.dto.request;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class JabatanRequestDto {

	private Long id;
	
	@NotBlank(message = "Field namaJabatan tidak boleh kosong")
	private String namaJabatan;
	@NotBlank(message = "Field gajiPokok tidak boleh kosong")
	private String gajiPokok;
	
}
