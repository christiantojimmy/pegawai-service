package com.wgs.pegawaiservice.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class PegawaiRequestDto {

	private Long id;
	
	@NotEmpty(message = "Field nip tidak boleh kosong")
	private String nip;
	@NotEmpty(message = "Field nama tidak boleh kosong")
	private String nama;
	@NotNull(message = "Field idJabatan tidak boleh kosong")
	private Long idJabatan;
	@NotNull(message = "Field idDepartemen tidak boleh kosong")
	private Long idDepartemen;
	
}
