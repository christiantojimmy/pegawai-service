package com.wgs.pegawaiservice.dto.request;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class DepartemenRequestDto {

	private Long id;
	
	@NotBlank(message = "Field nama departemen tidak boleh kosong")
	private String namaDepartemen;
	
}
