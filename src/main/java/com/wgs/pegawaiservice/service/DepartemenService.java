package com.wgs.pegawaiservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wgs.base.response.BaseErrorMessage;
import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.service.BaseService;
import com.wgs.pegawaiservice.dto.request.DepartemenRequestDto;
import com.wgs.pegawaiservice.dto.response.DepartemenResponseDto;
import com.wgs.pegawaiservice.model.Departemen;
import com.wgs.pegawaiservice.repository.DepartemenRepository;

@Service
public class DepartemenService implements BaseService<Departemen, Long, DepartemenRequestDto, DepartemenResponseDto> {

	@Autowired
	private DepartemenRepository repo;
	@Autowired
	private ModelMapper modelMapper;
	
	private Departemen base;
	private Departemen requestEntity;
	
	@Override
	public List<DepartemenResponseDto> viewAll() {
		List<Departemen> list = repo.findAll();
		List<DepartemenResponseDto> listDto = list.stream().map(a -> modelMapper.map(a, DepartemenResponseDto.class)).collect(Collectors.toList());
		return listDto;
	}

	@Override
	public boolean checkExist(BaseResponseMessage baseResponseMessage, DepartemenRequestDto request) {
		boolean isValid = true;
		
		base = repo.findById(request.getId()).orElse(null);
		
		if(base == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("id").message("Tidak dapat menemukan departemen dengan id " + request.getId()).build());
		}
		
		return isValid;
	}
	
	
	@Override
	public boolean beforeView(BaseResponseMessage baseResponseMessage, DepartemenRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean view(BaseResponseMessage baseResponseMessage, DepartemenRequestDto request) {
		baseResponseMessage.setData(modelMapper.map(base, DepartemenResponseDto.class));

		return true;
	}
	
	@Override
	public boolean save(BaseResponseMessage baseResponseMessage, DepartemenRequestDto request) {
		requestEntity = modelMapper.map(request, Departemen.class);
		repo.save(requestEntity);
		return true;
	}
	
	@Override
	public boolean beforeUpdate(BaseResponseMessage baseResponseMessage, DepartemenRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}

	@Override
	public boolean update(BaseResponseMessage baseResponseMessage, DepartemenRequestDto dto) {
		requestEntity = modelMapper.map(dto, Departemen.class);
		base.setForUpdate(requestEntity);
		repo.save(base);
		return true;
	}

	@Override
	public boolean beforeDelete(BaseResponseMessage baseResponseMessage, DepartemenRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean delete(BaseResponseMessage baseResponseMessage, DepartemenRequestDto request) {
		repo.softDelete(request.getId());
		
		return true;
	}
	
}
