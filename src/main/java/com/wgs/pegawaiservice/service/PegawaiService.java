package com.wgs.pegawaiservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wgs.base.response.BaseErrorMessage;
import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.service.BaseService;
import com.wgs.pegawaiservice.dto.request.PegawaiRequestDto;
import com.wgs.pegawaiservice.dto.response.PegawaiResponseDto;
import com.wgs.pegawaiservice.model.Departemen;
import com.wgs.pegawaiservice.model.Jabatan;
import com.wgs.pegawaiservice.model.Pegawai;
import com.wgs.pegawaiservice.repository.DepartemenRepository;
import com.wgs.pegawaiservice.repository.JabatanRepository;
import com.wgs.pegawaiservice.repository.PegawaiRepository;

@Service
public class PegawaiService implements BaseService<Pegawai, Long, PegawaiRequestDto, PegawaiResponseDto> {

	@Autowired
	private PegawaiRepository repo;
	@Autowired
	private JabatanRepository jabatanRepo;
	@Autowired
	private DepartemenRepository departemenRepo;
	@Autowired
	private ModelMapper modelMapper;
	
	private Pegawai base;
	private Pegawai requestEntity;
	
	@Override
	public List<PegawaiResponseDto> viewAll() {
		List<Pegawai> list = repo.findAll();
		List<PegawaiResponseDto> listDto = list.stream().map(a -> modelMapper.map(a, PegawaiResponseDto.class)).collect(Collectors.toList());
		return listDto;
	}

	@Override
	public boolean checkExist(BaseResponseMessage baseResponseMessage, PegawaiRequestDto request) {
		boolean isValid = true;
		
		base = repo.findById(request.getId()).orElse(null);
		
		if(base == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("id").message("Tidak dapat menemukan pegawai dengan id " + request.getId()).build());
		}
		
		return isValid;
	}
	
	@Override
	public boolean beforeView(BaseResponseMessage baseResponseMessage, PegawaiRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean view(BaseResponseMessage baseResponseMessage, PegawaiRequestDto request) {
		baseResponseMessage.setData(modelMapper.map(base, PegawaiResponseDto.class));

		return true;
	}
	
	@Override
	public boolean beforeSave(BaseResponseMessage baseResponseMessage, PegawaiRequestDto request) {
		boolean isValid = true;

		requestEntity = modelMapper.map(request, Pegawai.class);

		Jabatan jabatan = jabatanRepo.findById(request.getIdJabatan()).orElse(null);
		if(jabatan == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("idJabatan").message("Tidak dapat menemukan jabatan dengan id " + request.getIdJabatan()).build());
		} else {
			requestEntity.setJabatan(jabatan);
		}
		
		Departemen departemen = departemenRepo.findById(request.getIdDepartemen()).orElse(null);
		if(departemen == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("idDepartemen").message("Tidak dapat menemukan departemen dengan id " + request.getIdDepartemen()).build());
		} else {
			requestEntity.setDepartemen(departemen);
		}
		
		return isValid;
	}
	
	@Override
	public boolean save(BaseResponseMessage baseResponseMessage, PegawaiRequestDto request) {
		repo.save(requestEntity);
		return true;
	}
	
	@Override
	public boolean beforeUpdate(BaseResponseMessage baseResponseMessage, PegawaiRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		if(isValid) {
			requestEntity = modelMapper.map(request, Pegawai.class);
			
			Jabatan jabatan = jabatanRepo.findById(request.getIdJabatan()).orElse(null);
			if(jabatan == null) {
				isValid = false;
				baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("idJabatan").message("Tidak dapat menemukan jabatan dengan id " + request.getIdJabatan()).build());
			} else {
				requestEntity.setJabatan(jabatan);
			}
			
			Departemen departemen = departemenRepo.findById(request.getIdDepartemen()).orElse(null);
			if(departemen == null) {
				isValid = false;
				baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("idDepartemen").message("Tidak dapat menemukan departemen dengan id " + request.getIdDepartemen()).build());
			} else {
				requestEntity.setDepartemen(departemen);
			}
		}
		
		return isValid;
	}
	
	@Override
	public boolean update(BaseResponseMessage baseResponseMessage, PegawaiRequestDto dto) {
		base.setForUpdate(requestEntity);
		repo.save(base);
		return true;
	}

	@Override
	public boolean beforeDelete(BaseResponseMessage baseResponseMessage, PegawaiRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean delete(BaseResponseMessage baseResponseMessage, PegawaiRequestDto request) {
		repo.softDelete(request.getId());
		
		return true;
	}
	
}
