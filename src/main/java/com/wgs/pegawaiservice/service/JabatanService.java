package com.wgs.pegawaiservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wgs.base.response.BaseErrorMessage;
import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.service.BaseService;
import com.wgs.pegawaiservice.dto.request.JabatanRequestDto;
import com.wgs.pegawaiservice.dto.response.JabatanResponseDto;
import com.wgs.pegawaiservice.model.Jabatan;
import com.wgs.pegawaiservice.repository.JabatanRepository;

@Service
public class JabatanService implements BaseService<Jabatan, Long, JabatanRequestDto, JabatanResponseDto> {

	@Autowired
	private JabatanRepository repo;
	@Autowired
	private ModelMapper modelMapper;
	
	private Jabatan base;
	private Jabatan requestEntity;
	
	@Override
	public List<JabatanResponseDto> viewAll() {
		List<Jabatan> list = repo.findAll();
		List<JabatanResponseDto> listDto = list.stream().map(a -> modelMapper.map(a, JabatanResponseDto.class)).collect(Collectors.toList());
		return listDto;
	}

	@Override
	public boolean checkExist(BaseResponseMessage baseResponseMessage, JabatanRequestDto request) {
		boolean isValid = true;
		
		base = repo.findById(request.getId()).orElse(null);
		
		if(base == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("id").message("Tidak dapat menemukan jabatan dengan id " + request.getId()).build());
		}
		
		return isValid;
	}
	
	
	@Override
	public boolean beforeView(BaseResponseMessage baseResponseMessage, JabatanRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean view(BaseResponseMessage baseResponseMessage, JabatanRequestDto request) {
		baseResponseMessage.setData(modelMapper.map(base, JabatanResponseDto.class));

		return true;
	}
	
	@Override
	public boolean save(BaseResponseMessage baseResponseMessage, JabatanRequestDto request) {
		requestEntity = modelMapper.map(request, Jabatan.class);
		repo.save(requestEntity);
		return true;
	}
	
	@Override
	public boolean beforeUpdate(BaseResponseMessage baseResponseMessage, JabatanRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}

	@Override
	public boolean update(BaseResponseMessage baseResponseMessage, JabatanRequestDto dto) {
		requestEntity = modelMapper.map(dto, Jabatan.class);
		base.setForUpdate(requestEntity);
		repo.save(base);
		return true;
	}

	@Override
	public boolean beforeDelete(BaseResponseMessage baseResponseMessage, JabatanRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean delete(BaseResponseMessage baseResponseMessage, JabatanRequestDto request) {
		repo.softDelete(request.getId());
		
		return true;
	}
	
}
