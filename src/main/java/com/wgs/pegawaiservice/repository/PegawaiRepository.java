package com.wgs.pegawaiservice.repository;

import com.wgs.base.repository.BaseRepository;
import com.wgs.pegawaiservice.model.Pegawai;

public interface PegawaiRepository extends BaseRepository<Pegawai, Long> {

}
