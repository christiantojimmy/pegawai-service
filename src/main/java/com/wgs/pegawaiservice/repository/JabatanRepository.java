package com.wgs.pegawaiservice.repository;

import com.wgs.base.repository.BaseRepository;
import com.wgs.pegawaiservice.model.Jabatan;

public interface JabatanRepository extends BaseRepository<Jabatan, Long> {

}
