package com.wgs.pegawaiservice.repository;

import com.wgs.base.repository.BaseRepository;
import com.wgs.pegawaiservice.model.Departemen;

public interface DepartemenRepository extends BaseRepository<Departemen, Long> {

}
